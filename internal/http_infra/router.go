// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package http_infra

import (
	"context"
	"net/http"

	"github.com/prometheus/client_golang/prometheus/promhttp"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-service/internal/http_infra/metrics"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/cors"
	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"
	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-service/internal/repositories"
)

type key int

const (
	organizationRepositoryKey key = iota
	loggerKey                 key = iota
)

func NewRouter(organizationRepository repositories.OrganizationRepository, apiKey string, logger *zap.Logger) *chi.Mux {
	r := chi.NewRouter()

	cors := cors.New(cors.Options{
		AllowedMethods: []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
	})
	r.Use(cors.Handler)

	metricsMiddleware := metrics.NewMiddleware("scheme-service")
	r.Use(metricsMiddleware)
	r.Handle("/metrics", promhttp.Handler())

	r.Route("/v1", func(r chi.Router) {
		r.Use(middleware.SetHeader("API-Version", "1.0.0"))

		r.Route("/organizations", func(r chi.Router) {
			r.Use(middleware.Logger)

			r.Use(authenticatedOnly(apiKey, logger))

			r.Get("/", func(responseWriter http.ResponseWriter, request *http.Request) {
				ctx := context.WithValue(request.Context(), organizationRepositoryKey, organizationRepository)
				ctx = context.WithValue(ctx, loggerKey, logger)
				handlerList(responseWriter, request.WithContext(ctx))
			})
			r.Post("/", func(responseWriter http.ResponseWriter, request *http.Request) {
				ctx := context.WithValue(request.Context(), organizationRepositoryKey, organizationRepository)
				ctx = context.WithValue(ctx, loggerKey, logger)
				handlerCreate(responseWriter, request.WithContext(ctx))
			})
			r.Get("/{oin}", func(responseWriter http.ResponseWriter, request *http.Request) {
				ctx := context.WithValue(request.Context(), organizationRepositoryKey, organizationRepository)
				ctx = context.WithValue(ctx, loggerKey, logger)
				handlerGet(responseWriter, request.WithContext(ctx))
			})
			r.Put("/{oin}", func(responseWriter http.ResponseWriter, request *http.Request) {
				ctx := context.WithValue(request.Context(), organizationRepositoryKey, organizationRepository)
				ctx = context.WithValue(ctx, loggerKey, logger)
				handlerUpdate(responseWriter, request.WithContext(ctx))
			})
			r.Delete("/{oin}", func(responseWriter http.ResponseWriter, request *http.Request) {
				ctx := context.WithValue(request.Context(), organizationRepositoryKey, organizationRepository)
				ctx = context.WithValue(ctx, loggerKey, logger)
				handlerDelete(responseWriter, request.WithContext(ctx))
			})
		})

		r.Get("/openapi.json", func(responseWriter http.ResponseWriter, request *http.Request) {
			ctx := context.WithValue(request.Context(), loggerKey, logger)
			handlerJson(responseWriter, request.WithContext(ctx))
		})

		r.Get("/openapi.yaml", func(responseWriter http.ResponseWriter, request *http.Request) {
			ctx := context.WithValue(request.Context(), loggerKey, logger)
			handlerYaml(responseWriter, request.WithContext(ctx))
		})

		healthCheckHandler := healthcheck.NewHandler("scheme-service", []healthcheck.Checker{organizationRepository})
		r.Route("/health", func(r chi.Router) {
			r.Get("/", healthCheckHandler.HandleHealth)
			r.Get("/check", healthCheckHandler.HandlerHealthCheck)
		})
	})

	return r
}

func authenticatedOnly(apiKey string, logger *zap.Logger) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(responseWriter http.ResponseWriter, request *http.Request) {
			providedApiKey := request.Header.Get("Authentication")

			if providedApiKey != apiKey {
				logger.Log(zap.ErrorLevel, "Unauthorized: apikey invalid")
				http.Error(responseWriter, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
				return
			}
			next.ServeHTTP(responseWriter, request)
		})
	}
}
