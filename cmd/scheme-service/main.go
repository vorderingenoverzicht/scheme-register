// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package main

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net/http"

	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-service/internal/http_infra"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-service/internal/repositories"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-service/pkg/events"
)

type options struct {
	ListenAddress string `long:"listen-address" env:"LISTEN_ADDRESS" default:"0.0.0.0:80" description:"Address for the scheme-service api to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs."`
	APIKey        string `long:"api-key" env:"API_KEY" default:"" description:"Key to protect the API endpoints."`
	SchemeDB
	LogOptions
}

func main() {
	cliOptions, err := parseOptions()
	if err != nil {
		log.Fatalf("failed to parse options: %v", err)
	}

	logger, err := newLogger(cliOptions.LogOptions)
	if err != nil {
		log.Fatalf("failed to create logger: %v", err)
	}

	logger.Log(zap.DebugLevel, "setting up connection to scheme service db", zap.String("dsn", cliOptions.SchemeDB.DSNSafe()))
	schemeDB := SetupDBOrFatal(logger.With(zap.String("db", "scheme service")), cliOptions.SchemeDB)

	organizationRepository, err := repositories.NewOrganizationDBClient(context.Background(), logger, schemeDB)
	if err != nil {
		event := events.SCMS_3
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
	}

	router := http_infra.NewRouter(organizationRepository, cliOptions.APIKey, logger)

	event := events.SCMS_1
	logger.Log(event.GetLogLevel(), event.Message, zap.String("listenAddress", cliOptions.ListenAddress), zap.Reflect("event", event))
	err = http.ListenAndServe(cliOptions.ListenAddress, router)
	if err != nil {
		event = events.SCMS_2
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		if !errors.Is(err, http.ErrServerClosed) {
			panic(err)
		}
	}
}

func newLogger(logOptions LogOptions) (*zap.Logger, error) {
	config := logOptions.ZapConfig()
	logger, err := config.Build()
	if err != nil {
		return nil, fmt.Errorf("failed to create new zap logger: %v", err)
	}

	return logger, nil
}
