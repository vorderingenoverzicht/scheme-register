package events

import (
	"testing"

	"go.uber.org/zap/zapcore"
)

func Test_getLogLevel(t *testing.T) {
	type args struct {
		event *Event
	}
	tests := []struct {
		name string
		args args
		want zapcore.Level
	}{
		{"Fatal",
			args{NewEvent("TEST1", "test fatal level", VeryHigh)},
			zapcore.FatalLevel,
		},
		{"High",
			args{NewEvent("TEST2", "test high level", High)},
			zapcore.ErrorLevel,
		},
		{"Medium",
			args{NewEvent("TEST3", "test medium level", Medium)},
			zapcore.WarnLevel,
		},
		{"Low",
			args{NewEvent("TEST4", "test low level", Low)},
			zapcore.InfoLevel,
		},
		{"Unkown",
			args{NewEvent("TEST5", "Test unkown level", Unknown)},
			zapcore.InfoLevel,
		},
		{"Default",
			args{NewEvent("TEST6", "Test default case", "default")},
			zapcore.InfoLevel,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.args.event.GetLogLevel(); got != tt.want {
				t.Errorf("getLogLevel() = %v, want %v", got, tt.want)
			}
		})
	}
}
